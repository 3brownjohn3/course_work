﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using static ClassLib.ClassLib;

namespace ClassLib
{
    public class ClassLib
    {
        [Serializable]
        public class User
        {
            public string login;
            public string password;
        }
        [Serializable]
        public class Messages
        {
            public string login;
            public string message;
        }
    }

    public class Conn
    {
        private static Conn instance;
        private Conn() 
        { }
        public static Conn getInstance()
        {
            if (instance == null)
                instance = new Conn();
            return instance;
        }
        private static NetworkStream stream;
        byte[] data = new byte[64];
        public void connect()
        {
            const int port = 8888;
            const string address = "127.0.0.1";
            TcpClient client = null;
            client = new TcpClient(address, port);

            stream = client.GetStream();
        }
        public string receive()
        {
            byte[] ans = new byte[64]; // буфер для получаемых данных
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(ans, 0, ans.Length);
                builder.Append(Encoding.Unicode.GetString(ans, 0, bytes));
            } while (stream.DataAvailable);
            string message = builder.ToString();
            return message;
        }
        public string list_to_arr(List<Messages> hist, int i)
        {
            return (String.Format("{0}:{1}", hist[i].login, hist[i].message));
        }
        public List<Messages> receive_hist()
        {
            string mess = "4";
            byte[] data = Encoding.Unicode.GetBytes(mess);
            stream.Write(data, 0, data.Length);
            byte[] tmp = new Byte[99999];
            int bytes = 0;
            do
            {
                bytes = stream.Read(tmp, 0, tmp.Length);

            } while (stream.DataAvailable);
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();

            mStream.Write(tmp, 0, bytes);
            mStream.Position = 0;
            List<Messages> hist = new List<Messages>();
            mStream.Position = 0;
            hist = binFormatter.Deserialize(mStream) as List<Messages>;
            return hist;


        }
        public void send_message(string sms)
        {
            string mess = String.Format("3{0}", sms);
            byte[] data = Encoding.Unicode.GetBytes(mess);
            stream.Write(data, 0, data.Length);
        }

        public void send(int act, string log, string pass)
        {
            string message = String.Format("{0}{1}|{2}", act, log, pass);
            byte[] data = Encoding.Unicode.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }
        
    }

    public class ServerClass
    {
        public TcpClient client;
        public ServerClass(TcpClient tcpClient)
        {
            client = tcpClient;
        }
        public int user_id;
        string user_log;
        public void Process()
        {
            NetworkStream stream = null;
            stream = client.GetStream();
            byte[] data = new byte[64]; // буфер для получаемых данных
            while (true)
            {
                // получаем сообщение
                StringBuilder builder = new StringBuilder();
                try
                {
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    } while (stream.DataAvailable);
                }
                catch
                {
                    Console.WriteLine("User terminated the connection");
                    return;
                }
                string message = builder.ToString();
                Console.WriteLine(message);
                int act = int.Parse(message[0].ToString());
                int index = message.IndexOf('|');
                index++;
                string sms;
                switch (act)
                {
                    case 1:
                        string log = message.Substring(1, (index - 2));
                        string pass = message.Substring(index, (message.Length - index));
                        user_log = log;
                        sms = login_proc(log, pass);
                        data = Encoding.Unicode.GetBytes(sms);
                        stream.Write(data, 0, data.Length);
                        break;
                    case 2:
                        string logi = message.Substring(1, (index - 2));
                        string passa = message.Substring(index, (message.Length - index));
                        user_log = logi;
                        sms = register_proc(logi, passa);
                        data = Encoding.Unicode.GetBytes(sms);
                        stream.Write(data, 0, data.Length);
                        break;
                    case 3:
                        string user_sms = message.Substring(1, (message.Length - 1));
                        user_message_send(user_log, user_sms);
                        break;
                    case 4:
                        user_message_show(stream);
                        break;
                    default:
                        break;
                }
            }
        }

        public string login_proc(string log, string pass)
        {
            string path_info = "E:/ALL WITH STUDY AND WORK/kursach/MessengerLoginForm/data/info_users.data";
            string sms = "123";
            List<User> info = new List<User>();
            using (var file = File.OpenWrite(path_info))
            {
                if (file.Length == 0)
                {
                    info.Add(new User { login = "admin", password = "admin" });
                    var writer = new BinaryFormatter();
                    writer.Serialize(file, info); // Writes the entire list.
                }
            }
            using (var file = File.OpenRead(path_info))
            {
                var reader = new BinaryFormatter();
                info = (List<User>)reader.Deserialize(file); // Reads the entire list.
                for (int i = 0; i < info.Count; i++)
                {
                    if (info[i].login == log && info[i].password == pass)
                    {
                        sms = "Successfull login.";
                        break;
                    }
                    else
                    {
                        sms = "Wrong password or login.";
                    }
                }
            }
            return sms;
        }
        public string register_proc(string log, string pass)
        {
            string path_info = "E:/ALL WITH STUDY AND WORK/kursach/MessengerLoginForm/data/info_users.data";
            string sms;

            List<User> data = new List<User>();
            using (var file = File.OpenRead(path_info))
            {
                var reader = new BinaryFormatter();
                data = (List<User>)reader.Deserialize(file); // Reads the entire list.

                if (data.Exists(x => x.login == log))
                {
                    sms = "Login already used. Try another.";
                    return sms;
                }
            }
            using (var file = File.OpenWrite(path_info))
            {
                data.Add(new User { login = log, password = pass });
                var writer = new BinaryFormatter();
                writer.Serialize(file, data); // Writes the entire list.
                sms = "Succesfull registration.";
            }
            return sms;
        }

        public void user_message_send(string user_log, string sms)
        {
            string path_info = "E:/ALL WITH STUDY AND WORK/kursach/MessengerLoginForm/data/history.data";
            List<Messages> hist = new List<Messages>();

            using (var file = File.OpenRead(path_info))
            {
                if (file.Length != 0)
                {
                    var reader = new BinaryFormatter();
                    hist = (List<Messages>)reader.Deserialize(file); // Reads the entire list.
                }
            }
            hist.Add(new Messages { login = user_log, message = sms });
            using (var file = File.OpenWrite(path_info))
            {
                var writer = new BinaryFormatter();
                writer.Serialize(file, hist); // Writes the entire list.
            }
        }

        public void user_message_show(NetworkStream stream)
        {
            string path_info = "E:/ALL WITH STUDY AND WORK/kursach/MessengerLoginForm/data/history.data";
            List<Messages> hist = new List<Messages>();
            byte[] data = new byte[64];
            using (var file = File.OpenRead(path_info))
            {
                if (file.Length != 0)
                {
                    var reader = new BinaryFormatter();
                    hist = (List<Messages>)reader.Deserialize(file); // Reads the entire list.
                }
                else
                {
                    hist.Add(new Messages { login = "admin", message = "Hi, chat!" });
                }

                var binFormatter = new BinaryFormatter();
                var mStream = new MemoryStream();
                binFormatter.Serialize(mStream, hist);
                mStream.Position = 0;

                stream.Write(mStream.ToArray(), 0, mStream.ToArray().Length);
            }

        }

    }
}
