﻿using ClassLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static ClassLib.ClassLib;

namespace Messenger
{
    public partial class MainWin : Form
    {
        Conn objConn = Conn.getInstance();
        public MainWin()
        {
            InitializeComponent();
            print();
        }

        public void print()
        {
            richTextBoxHistory.Text = "";
            List<Messages> hist = objConn.receive_hist();
            string[] arr = new string[hist.Count];
            for (int i = 0; i < hist.Count; i++)
            {
                arr[i] = objConn.list_to_arr(hist, i);
                richTextBoxHistory.Text += arr[i] + "\n";
            }
            richTextBoxHistory.ReadOnly = true;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            objConn.send_message(textBoxSendMess.Text);
            textBoxSendMess.Clear();
        }

        private void MainWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            print();
        }
    }
}
