﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using System.Net.Sockets;
using ClassLib;
using System.Reflection;
using System.Net.Http.Headers;

namespace Messenger
{
    public partial class StarterForm : Form
    {
        Conn objConn = Conn.getInstance();
        public StarterForm()
        {
            InitializeComponent();
            try
            {
                objConn.connect();
            }
            catch(Exception e)
            {
                MessageBox.Show("Сan not connect ot server.Try again");
                Environment.Exit(0);
            }
        }
        private void Login_Form_Load(object sender, EventArgs e)
        {

        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void buttonLog_Click(object sender, EventArgs e)
        {
            objConn.send(1, textBoxUserName.Text, textBoxPassword.Text);
            string message = objConn.receive();
            if (message == "Wrong password or login.")
            {
                MessageBox.Show(message);
            }
            else if (message == "Successfull login.")
            {
                MessageBox.Show(message);

                MainWin main = new MainWin();
                this.Hide();
                main.Show();
            }
        }


        private void buttonReg_Click(object sender, EventArgs e)
        {
            panelReg.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // register button
            
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonBackToLogin_Click_1(object sender, EventArgs e)
        {
            panelReg.Visible = false;
        }

        private void buttonExit1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (textBoxPass.Text == textBoxPassConf.Text)
            {
                objConn.send(2, textBoxLogin.Text, textBoxPass.Text);
                string message = objConn.receive();
                if (message == "Login already used. Try another.")
                {
                    MessageBox.Show(message);
                }
                else if (message == "Succesfull registration.")
                {
                    MessageBox.Show(message);
                    MainWin main = new MainWin();
                    Hide();
                    main.Show();
                }
            }
            else
            {
                string message = "Error. Passwords do not match";
                MessageBox.Show(message);
            }
        }

        private void buttonShowPass_Click_1(object sender, EventArgs e)
        {
            if (textBoxPassword.PasswordChar == '*')
            {

                textBoxPassword.PasswordChar = '\0';
            }
            else
            {
                textBoxPassword.PasswordChar = '*';
            }
        }

        private void buttonExit_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBoxLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPass_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPassConf_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
